library(tm)
library(RTextTools)
library(e1071)
library(tidyr)
library(dplyr)
library(caret)

NaiveBayesClassifier <- function() {
  df<- read.csv("data/labeledTrainData.tsv", header = TRUE, stringsAsFactors = FALSE, sep = "\t")
  
  set.seed(5318008)
  
  df <- df[sample(nrow(df)), c('sentiment', 'review')]
  
  df$sentiment <- as.factor(df$sentiment)
  
  corpus <- Corpus(VectorSource(df$review))
  
  corpus.clean <- corpus %>%
    tm_map(content_transformer(tolower)) %>% 
    tm_map(removePunctuation) %>%
    tm_map(removeNumbers) %>%
    tm_map(removeWords, stopwords(kind="en")) %>%
    tm_map(stripWhitespace)
  
  dtm <- DocumentTermMatrix(corpus.clean)
  
  perc <- 75
  
  perc_75 <- (nrow(df)/100)*perc
  df.train <- df[1:perc_75,]
  dtm.train <- dtm[1:perc_75,]

  corpus.clean.train <- corpus.clean[1:perc_75]

  mostFrequent <- findFreqTerms(dtm.train, 15*perc)
  
  getCleanedMatrix <- function(frame) {
    corpus <- Corpus(VectorSource(frame))
    corpus.clean <- corpus %>%
      tm_map(content_transformer(tolower)) %>% 
      tm_map(removePunctuation) %>%
      tm_map(removeNumbers) %>%
      tm_map(removeWords, stopwords(kind="en")) %>%
      tm_map(stripWhitespace)
    
    DocumentTermMatrix(corpus.clean)
  }
  
  dtm.train.nb <- DocumentTermMatrix(corpus.clean.train, control=list(dictionary = mostFrequent))

  convert_count <- function(x) {
    y <- ifelse(x > 0, 1,0)
    y <- factor(y, levels=c(0,1), labels=c("No", "Yes"))
    y
  }
  
  trainNB <- apply(dtm.train.nb, 2, convert_count)
  
  classifier <- naiveBayes(trainNB, df.train$sentiment) 
  
  frameClassifier <- function(frame) {
    test.matrix <- apply(getCleanedMatrix(frame$review), 2, convert_count)
    pred <- predict(classifier, newdata=test.matrix)
    table("Predictions"= pred,  "Actual" = frame$sentiment )
    conf.mat <- confusionMatrix(pred, frame$sentiment)
    conf.mat
    conf.mat$overall['Accuracy']
  }
  
  frameClassifierWithoutSentiment <- function(frame) {
    test.matrix <- apply(getCleanedMatrix(frame$review), 2, convert_count)
    predict(classifier, newdata=test.matrix)
  }
  
  list(
    classifyFrame = function(frame) frameClassifier(frame),
    classifyFrameWithout = function(frame) frameClassifierWithoutSentiment(frame)
  )
}
